package com.example.myexamthread

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myexamthread.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var handler = Handler(Looper.getMainLooper())
    private val MESSAGE_COUNT_DOWN = 1001
    private val MESSAGE_DONE = 1002


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initHandler()
    }

    private fun initHandler() {
        handler = object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    MESSAGE_COUNT_DOWN
                    -> binding.txtNumber.text = msg.arg1.toString()
                    MESSAGE_DONE
                    -> Toast.makeText(this@MainActivity,"Done!",Toast.LENGTH_SHORT).show()
                    else
                    -> Toast.makeText(this@MainActivity,"Error",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun initView() {
        binding.btnCount.setOnClickListener {
            doCountDown()
        }
    }

    private fun doCountDown() {
        Thread(Runnable {
            var time = 10
            do {
                time--

                val msg = Message()
                msg.what = MESSAGE_COUNT_DOWN
                msg.arg1 = time
                handler.sendMessage(msg)

                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            } while (time > 0)
                handler.sendEmptyMessage(MESSAGE_DONE)
        }).start()

    }
}














//fun main(args:Array<String>) {
//    val t1 = UserThread("Thread 1")
//    t1.start()
//
//    val t2 = UserThread("Thread 2")
//    t2.start()
//
//    val t3 = UserThread("Thread 3")
//    t3.start()
//    println("Thread is run")
//}
//class UserThread() : Thread() {
//    var threadName : String = ""
//
//    constructor(ThreadName:String) : this(){
//        this.threadName = ThreadName
//        println(this.threadName + " is started")
//    }
//
//    override fun run() {
//        var count = 0
//        while (count < 10) {
//            println(this.threadName + " Count:$count")
//            count++
//            try {
//                Thread.sleep(1000)
//            } catch(ex:Exception) {
//                print(ex.message)
//            }
//        }
//    }
//}